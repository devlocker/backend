require "test_helper"

class SessionsHelperTest < ActionView::TestCase
  def setup
    basic_setup
  end

  test "sign_in" do
    sign_in(@user)

    assert_equal @user.id, session[:user_id]
  end

  test "signed_in? when a user is logged in" do
    sign_in(@user)

    assert signed_in?
  end

  test "signed_in? when a user is not logged in" do
    refute signed_in?
  end

  test "current_user returns the logged in user" do
    sign_in(@user)

    assert_equal @user, current_user
  end

  test "current_user returns a guest user if no one is logged in" do
    assert_instance_of GuestUser, current_user
  end
end
