require "test_helper"

class AddonTest < ActiveSupport::TestCase
  def setup
    basic_setup
  end

  def test_post_create
    stub_request(
      :post,
      "https://root.atlassian.net/rest/auth/1/session"
    ).to_return(
      status: 200,
      body: "",
      headers: {
        set_cookie: [0, 1, 2, 3, 4]
      }
    )

    addon = FactoryGirl.create(
      :addon,
      locker: @locker,
      name: "jira",
      metadata: {
        domain: "https://root.atlassian.net"
      }
    )

    addon.post_create("username", "password")

    # Index 0, 1, and 4 are the headers that we save off
    assert "014", addon.reload.metadata["cookie"]
  end

  def test_metadata_validator
    addon = Addon.create(
      name: "jira",
      locker: @locker,
      metadata: {
        domain: "http://root.atlassian.net"
      }
    )

    assert_nil addon.id
    assert_equal(
      ["Please use the https protocol for domains"],
      addon.errors.messages[:metadata]
    )

    addon.metadata = { domain: "https://root.atlassian.net" }
    addon.save

    assert addon.id
  end
end
