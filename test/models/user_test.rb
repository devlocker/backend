require "test_helper"

class UserTest < ActiveSupport::TestCase
  def test_name
    basic_setup
    assert_equal "Example User", @user.name
  end

  def test_name_default
    user = User.new(email: "user@example.com")

    assert_equal "user@example.com", user.name
  end
end
