require "test_helper"

class SessionsControllerTest < ActionController::TestCase
  def setup
    @user = User.create(email: "user@example.com", password: "secret")
  end

  test "valid credentials sign in" do
    post :create, session: { email: @user.email, password: @user.password }

    assert_response :redirect, lockers_path
  end

  test "invalid credentials re-render login" do
    post :create, session: { email: @user.email, password: "wrong_password" }

    assert_template "new"
  end

  test "signout" do
    session[:user_id] = @user.id
    delete :destroy

    assert_equal nil, session[:user_id]
  end
end
