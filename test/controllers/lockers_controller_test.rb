require "test_helper"

class LockersControllerTest < ActionController::TestCase
  def setup
    basic_setup
  end

  test "should get index" do
    get :index, {}, user_id: @user.to_param

    assert_equal @user.lockers, assigns(:lockers)
  end

  test "should get new" do
    get :new, {}, user_id: @user.to_param

    assert_instance_of Locker, assigns(:locker)
  end

  test "should get show" do
    get :show, { id: @locker.to_param }, user_id: @user.to_param

    assert_equal @locker.addons.enabled, assigns(:enabled_addons)
  end

  test "create should create a locker for the current user" do
    post :create, { locker: { name: "Locker" } }, user_id: @user.to_param

    refute_nil assigns(:locker).id
  end

  test "destroy should find and destroy the targeted locker" do
    delete :destroy, { id: @locker.to_param }, user_id: @user.to_param

    assert_equal 0, @user.lockers.count
  end
end
