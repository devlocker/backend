require "test_helper"

class Github::PullRequestsControllerTest < ActionController::TestCase
  def setup
    basic_setup

    stub_request(
      :get,
      "https://api.github.com/issues?filter=created"
    ).to_return(
      status: 200,
      body: File.read(Rails.root.join("test", "fixtures", "github", "issues_response.json")),
      headers: {}
    )
  end

  test "index should gather pull requests" do
    FactoryGirl.create(
      :addon,
      locker: @locker,
      name: "github",
      metadata: {
        "connected_repo_ids" => [1]
      }
    )

    get :index, { locker_id: @locker.id, format: :json }, user_id: @user.id

    refute_nil assigns(:pull_requests)
  end

  test "index shouldn't fail if there are no connected repos" do
    FactoryGirl.create(
      :addon,
      locker: @locker,
      name: "github",
      metadata: {
        "connected_repo_ids" => nil
      }
    )

    get :index, { locker_id: @locker.id, format: :json }, user_id: @user.id

    refute_nil assigns(:pull_requests)
  end

  def repos
    [
      {
        "name" => "devlocker",
        "id" => 1
      },
      {
        "name" => "parcel",
        "id" => 2
      }
    ]
  end
end
