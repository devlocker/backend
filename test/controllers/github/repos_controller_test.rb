require "test_helper"

class Github::ReposControllerTest < ActionController::TestCase
  def setup
    basic_setup
    @addon = FactoryGirl.create(:addon, locker: @locker, name: "github")

    @params = {
      locker_id: @locker.to_param,
      format: :json
    }
  end

  def github_client
    stub(repos: repos)
  end

  def repos
    [
      OpenStruct.new(
        id: 1,
        name: "devlocker/devlocker"
      )
    ]
  end

  test "GET #index" do
    GithubAddon::Client.stub(:new, github_client) do
      get :index, @params, user_id: @user.to_param

      assert_equal repos, assigns(:repos)
    end
  end

  test "PUT #update with new repo to add" do
    put(
      :update,
      @params.merge(locker_membership: true, id: 1337),
      user_id: @user.to_param
    )

    @addon.reload
    assert_equal [1337], @addon.metadata["connected_repo_ids"]
  end

  test "PUT #update with repo to remove" do
    @addon.metadata["connected_repo_ids"] = [1338, 1339]
    @addon.save

    put(
      :update,
      @params.merge(locker_membership: false, id: 1338),
      user_id: @user.to_param
    )

    @addon.reload
    assert_equal [1339], @addon.metadata["connected_repo_ids"]
  end
end
