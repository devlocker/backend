require "test_helper"

class UsersControllerTest < ActionController::TestCase
  def setup
    basic_setup
  end

  test "creating a user through a json post request" do
    assert_difference("User.count") do
      post :create,
        user: {
          email: "new-user@example.com",
          first_name: "New",
          last_name: "User",
          password: "passw0rd",
          password_confirmation: "passw0rd"
        },
        format: :json
    end
  end

  test "creating a user with invalid params" do
    post :create,
      user: {
        email: "new-user@example.com",
        first_name: "New",
        last_name: "User",
        password: "passw0rd",
        password_confirmation: "wrong-passw0rd"
      },
      format: :json

    assert_equal 400, response.status
  end

  test "editing a user" do
    get :edit,
      {
        id: @user.to_param,
        locker_id: @locker.to_param
      },
      user_id: @user.to_param

    assert_template :edit
  end

  test "updating a user with valid params" do
    email = "new_email@example.com"
    patch :update,
      {
        id: @user.to_param,
        locker_id: @locker.to_param,
        user: {
          email: email
        }
      },
      user_id: @user.to_param

    assert_equal 302, response.status
    assert_equal email, @user.reload.email
  end

  test "updating a user with invalid params" do
    patch :update,
      {
        id: @user.to_param,
        locker_id: @locker.to_param,
        user: {
          password: "passw0rd",
          password_confirmation: "wrong-passw0rd"
        }
      },
      user_id: @user.to_param

    assert_template :edit
  end

  test "validate email with existing email" do
    get :validate_email, { email: @user.email }, user_id: @user.to_param

    assert_equal 200, response.status
    assert_equal true, JSON.parse(response.body)["exists"]
  end

  test "validate email with new email" do
    get :validate_email, { email: "new-email@example.com" }, user_id: @user.to_param

    assert_equal 200, response.status
    assert_equal false, JSON.parse(response.body)["exists"]
  end
end
