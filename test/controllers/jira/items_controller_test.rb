require "test_helper"

class Jira::ItemsControllerTest < ActionController::TestCase
  def setup
    basic_setup
    @addon = FactoryGirl.create(:addon, locker: @locker, name: "jira")

    @params = {
      locker_id: @locker.to_param,
      format: :json
    }
  end

  def jira_client
    stub(my_issues: items)
  end

  def items
    [
      {
        "fields" => {
          "description" => "Why would you do that?",
          "summary" => "Summer BBQ",
        }
      }
    ]
  end

  test "GET #index" do
    Jira::Client.stub(:new, jira_client) do
      get :index, @params, user_id: @user.to_param

      assert_equal items, assigns(:items)
    end
  end
end
