require "test_helper"

class AuthenticationsControllerTest < ActionController::TestCase
  def setup
    basic_setup

    Rails.application.routes.draw do
      resources :lockers, only: [:show]
      post :create, to: "authentications#create"
    end
  end

  def teardown
    Rails.application.reload_routes!
  end

  test "create should add authentication information to a new addon" do
    @controller.stub :omniauth_hash, auth_hash do
      post :create, { locker_id: @locker.id }, user_id: @user.id

      addon = @locker.addons.last

      assert "google", addon.name
      assert "refresh_token", addon.api_token
      assert "username", addon.metadata["username"]
    end
  end

  test "create should update an existing addon with authentication information" do
    addon = FactoryGirl.create(:addon, locker: @locker, name: "google", api_token: "token")

    @controller.stub :omniauth_hash, auth_hash do
      post :create, { locker_id: @locker.id }, user_id: @user.id

      assert "refresh_token", addon.reload.api_token
    end
  end

  test "create should redirect to lockers#show" do
    @controller.stub :omniauth_hash, auth_hash do
      post :create, { locker_id: @locker.id }, user_id: @user.id

      assert_response :redirect
    end
  end
end

def auth_hash
  {
    credentials: {
      refresh_token: "refresh_token"
    },
    provider: "google",
    info: {
      nickname: "username"
    }
  }
end
