require "test_helper"

class Todo::ItemsControllerTest < ActionController::TestCase
  def setup
    basic_setup
    @item = FactoryGirl.create(:todo_item, locker: @locker)

    @params = {
      locker_id: @locker.to_param,
      format: :json
    }
  end

  test "should get index" do
    FactoryGirl.create(:todo_item)

    get :index, @params, user_id: @user.to_param

    assert_equal [@item], assigns(:items)
  end

  test "should get show" do
    get :show, @params.merge(id: @item.to_param), user_id: @user.to_param

    assert_equal @item, assigns(:item)
  end

  test "should post create" do
    post(
      :create,
      @params.merge(
        item: {
          content: "Content"
        }
      ),
      user_id: @user.to_param
    )

    assert_kind_of Integer, assigns(:item).id
  end

  test "should patch update" do
    patch(
      :update,
      @params.merge(
        id: @item.to_param,
        item: {
          content: "Updated Content"
        }
      ),
      user_id: @user.to_param
    )

    assert_equal "Updated Content", assigns(:item).content
  end

  test "should delete destroy" do
    assert_difference "Todo::Item.count", -1 do
      delete :destroy, @params.merge(id: @item.to_param), user_id: @user.to_param
    end
  end
end
