require "test_helper"

class ResetPasswordControllerTest < ActionController::TestCase
  def setup
    basic_setup
  end

  test "new renders the new template" do
    get :new

    assert_template :new
  end

  test "create changes the user's password to a random string" do
    old_digest = @user.password_digest

    post :create, reset: { email: @user.email }

    refute_equal old_digest, @user.reload.password_digest
  end

  test "create sends a reset email when successful" do
    post :create, reset: { email: @user.email }

    refute_empty ActionMailer::Base.deliveries
  end

  test "create redirects to the signin path when successful" do
    post :create, reset: { email: @user.email }

    assert_redirected_to "/signin"
  end

  test "create redirects to new when unsuccessful" do
    post :create, reset: { email: "invalid" }

    assert_redirected_to reset_password_path
  end
end
