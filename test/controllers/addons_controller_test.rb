require "test_helper"

class AddonsControllerTest < ActionController::TestCase
  def setup
    basic_setup

    @addon = FactoryGirl.create(:addon, locker: @locker, enabled: true)
  end

  test "should get index" do
    get :index, { locker_id: @locker.id }, user_id: @user.to_param

    assert_equal @locker.addons.pluck(:name), assigns(:addons)
  end

  test "installed should only return enabled addons" do
    enabled_addon = FactoryGirl.create(:addon, enabled: true, locker: @locker)
    disabled_addon = FactoryGirl.create(:addon, enabled: false, locker: @locker)

    get :installed, { locker_id: @locker.id }, user_id: @user.to_param

    assert_includes assigns(:addons), enabled_addon
    refute_includes assigns(:addons), disabled_addon
  end

  test "should get show" do
    get :show, { locker_id: @locker.id, id: @addon.id }, user_id: @user.to_param

    assert_equal 200, response.status
  end

  test "should post create" do
    post(
      :create,
      {
        locker_id: @locker.id,
        addon: {
          name: "trello",
          metadata: {
            username: "username",
            password: "password",
          }
        }
      },
      user_id: @user.to_param
    )

    assert_response :redirect, locker_path(@locker)
    assert_nil assigns(:addon).metadata[:password]
  end

  test "should delete destroy" do
    delete(
      :destroy,
      {
        locker_id: @locker.id,
        id: @addon.id
      },
      user_id: @user.to_param
    )

    refute @addon.reload.enabled?
  end
end
