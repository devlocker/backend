require "test_helper"

class GoogleCalendar::ItemsControllerTest < ActionController::TestCase
  def setup
    basic_setup
    @addon = FactoryGirl.create(:addon, locker: @locker, name: "google")

    @params = {
      locker_id: @locker.to_param,
      format: :json
    }
  end

  def google_calendar_client
    stub(events: items)
  end

  def items
    [
      OpenStruct.new(
        id: 1,
        summary: "Summer BBQ",
        start: OpenStruct.new(date_time: Time.at(0))
      )
    ]
  end

  test "GET #index" do
    GoogleCalendar::Client.stub(:new, google_calendar_client) do
      get :index, @params, user_id: @user.to_param

      assert_equal items, assigns(:items)
    end
  end
end
