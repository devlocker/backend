require "test_helper"

class TestController < ApiController
  skip_before_action :authorize

  def index
    cache("cache_key") do
      [1, 2, 3]
    end
    head :ok
  end

  def expire
    cache("cache_key") do
      [4, 5, 6]
    end
    head :ok
  end

  def index2
    cache("cache_key/#{params[:user_id]}") do
      params[:user_id]
    end
    head :ok
  end
end

class TestControllerTest < ActionController::TestCase
  def setup
    Rails.cache.clear
    Rails.application.routes.draw do
      resources :test do
        collection do
          get :expire
          get :index2
        end
      end
    end
  end

  def teardown
    Rails.application.reload_routes!
    Rails.cache.clear
  end

  test "#cache caches the results of the block to the given cache_key" do
    get :index

    assert_equal [1, 2, 3], Rails.cache.fetch("cache_key")
  end

  test "#cache won't find the cache when given the wrong cache_key" do
    get :index

    assert_nil Rails.cache.fetch("wrong_cache_key")
  end

  test "#cache can cache different results for the same action from different users" do
    get :index2, user_id: 1
    get :index2, user_id: 2

    assert_equal "1", Rails.cache.fetch("cache_key/1")
    assert_equal "2", Rails.cache.fetch("cache_key/2")
  end

  test "#cache will manually expire the cache and set the new value if passed manual_expire: true" do
    get :index

    assert_equal [1, 2, 3], Rails.cache.fetch("cache_key")

    get :expire, manual_expire: true

    assert_equal [4, 5, 6], Rails.cache.fetch("cache_key")
  end

  test "#cache can determine the manual_expire boolean from a string" do
    get :index

    assert_equal [1, 2, 3], Rails.cache.fetch("cache_key")

    get :expire, manual_expire: "false"

    assert_equal [1, 2, 3], Rails.cache.fetch("cache_key")

    get :expire, manual_expire: "true"

    assert_equal [4, 5, 6], Rails.cache.fetch("cache_key")
  end

  test "#cache will interpret anything other than some variation of true as false" do
    get :index

    assert_equal [1, 2, 3], Rails.cache.fetch("cache_key")

    get :expire, manual_expire: "other"

    assert_equal [1, 2, 3], Rails.cache.fetch("cache_key")

    get :expire, manual_expire: ""

    assert_equal [1, 2, 3], Rails.cache.fetch("cache_key")
  end
end
