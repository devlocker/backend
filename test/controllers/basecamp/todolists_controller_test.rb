require "test_helper"

class Basecamp::TodolistsControllerTest < ActionController::TestCase
  def setup
    basic_setup
    FactoryGirl.create(:addon, name: "basecamp", locker: @locker)
  end

  test "should get index" do
    Basecamp::Client.stub(:new, OpenStruct.new(todolists: todolists)) do
      get :index, { locker_id: @locker.to_param, format: :json }, user_id: @user.to_param

      assert_equal todolists, assigns(:todolists)
    end
  end

  def todolists
    [
      {
        "id" => 1,
        "name" => "Things to do today",
        "app_url" => "https://basecamp.com/todolists/1",
      }
    ]
  end
end
