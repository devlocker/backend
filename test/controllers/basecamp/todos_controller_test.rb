require "test_helper"

class Basecamp::TodosControllerTest < ActionController::TestCase
  def setup
    basic_setup
    FactoryGirl.create(:addon, name: "basecamp", locker: @locker)
  end

  test "should get index" do
    Basecamp::Client.stub(:new, mock(todos: todos)) do
      get :index, { locker_id: @locker.to_param, format: :json }, user_id: @user.to_param

      assert_equal todos, assigns(:todos)
    end
  end

  def todos
    [
      {
        "todolist" => [
          {
            "name" => "todo"
          }
        ]
      }
    ]
  end
end
