require "test_helper"

class Basecamp::OauthControllerTest < ActionController::TestCase
  def setup
    basic_setup

    stub_request(
      :post,
      "https://launchpad.37signals.com/authorization/token?client_id=test&" +
      "client_secret=test&code=&redirect_uri=" +
      "http://devlocker.test/basecamp/oauth&type=web_server"
    ).to_return(
      status: 200,
      body: File.read(
        Rails.root.join(
          "test",
          "fixtures",
          "basecamp",
          "oauth_response.json"
        )
      ),
      headers: {
        "Content-Type" => "application/json"
      }
    )
  end

  test "new should set locker_id in the session and redirect" do
    get :new, { locker_id: 1 }, user_id: @user.to_param

    assert_equal "1", session[:locker_id]
    assert_response :redirect
  end

  test "create should update the basecamp addon with access/refresh tokens" do
    session[:locker_id] = @locker.id
    basecamp = FactoryGirl.create(:addon, name: "basecamp", locker: @locker)

    post :create, {}, user_id: @user.to_param

    assert_equal "fake_access_token", basecamp.reload.api_token
    assert_equal "fake_refresh_token", basecamp.metadata["refresh_token"]
  end

  test "create should make an addon for the user if one doesn't exist" do
    session[:locker_id] = @locker.id
    refute @locker.addons.find_by(name: "basecamp", enabled: true)

    post :create, {}, user_id: @user.to_param

    assert @locker.addons.find_by(name: "basecamp", enabled: true)
  end

  test "create should delete locker_id from the session and redirect" do
    session[:locker_id] = @locker.id
    FactoryGirl.create(:addon, name: "basecamp", locker: @locker)

    post :create, {}, user_id: @user.to_param

    refute session[:locker_id]
    assert_response :redirect
  end
end
