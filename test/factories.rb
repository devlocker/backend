FactoryGirl.define do
  factory :user do
    sequence(:email) { |n| "user#{n}@example.com" }
    password "digest"
    password_confirmation "digest"
    first_name "Example"
    last_name "User"
  end

  factory :locker do
    name "Locker"
  end

  factory :addon do
    name "Addon"
  end

  factory :todo_item, class: Todo::Item do
    content "Content"
    completed true
  end
end
