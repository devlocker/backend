ENV["RAILS_ENV"] = "test"
require File.expand_path("../../config/environment", __FILE__)
require "rails/test_help"
require "minitest/rails"
require "minitest/pride"
require "webmock/minitest"
require "minitest/rails/capybara"
require "mocha/mini_test"
require "factory_girl_rails"

module DatabaseCleanerHelper
  def before_setup
    DatabaseCleaner.strategy = :truncation
    DatabaseCleaner.start
    super
  end

  def after_teardown
    DatabaseCleaner.clean
    super
  end
end

module Devlocker
  class TestCase < Minitest::Test
    include DatabaseCleanerHelper
  end
end

module ActionController
  class TestCase
    include DatabaseCleanerHelper
  end
end

module Capybara
  module Rails
    class TestCase
      def signin(user)
        visit signin_path

        fill_in "Email", with: user.email
        fill_in "Password", with: user.password
        click_button "Sign In"
      end

      def create_locker
        visit new_locker_path

        fill_in "Name", with: "ticketlocker-locker"
        click_on "Save"
        visit lockers_path
        click_on "ticketlocker-locker"
      end

      def install_apps
        click_on "Install More apps"
        click_on "Trello"
        click_on "Install"

        click_on "Install More apps"
        click_on "Github"
        click_on "Install"
        visit lockers_path
        click_on "ticketlocker-locker"
      end
    end
  end
end

class ActiveSupport::TestCase
  ActiveRecord::Migration.check_pending!
  fixtures :all

  def json
    @json ||= JSON.parse(response.body, object_class: OpenStruct)
  end

  def basic_setup
    @user = FactoryGirl.create(:user)
    @locker = FactoryGirl.create(:locker, user: @user)
  end

  def addons_auth
    OmniAuth.config.test_mode = true
    OmniAuth.config.mock_auth[:trello] = {
      "provider" => "trello",
      "uid" => "123545",
      "credentials" => {
        "token" => "1h2ki139o8bt"
      }
    }

    OmniAuth.config.mock_auth[:github] = {
      "provider" => "github",
      "uid" => "123545",
      "credentials" => {
        "token" => "3df4e5a9be8c06b0eb8c132cb4e6ecfd5b628ce6"
      }
    }
  end
end
