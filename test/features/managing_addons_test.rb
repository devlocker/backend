require "test_helper"

class ManagingAddonsTest < Capybara::Rails::TestCase
  def setup
    basic_setup
  end

  test "Installing Jira" do
    signin(@user)

    visit locker_addons_path(@locker)
    click_on "Jira"

    fill_in "Username", with: "user"
    fill_in "Password", with: "password"
    fill_in "Domain", with: "https://jira.com"
  end

  test "Installing Basecamp" do
    signin(@user)

    visit locker_addons_path(@locker)
    click_on "Basecamp"

    assert_content "Integrate with your Basecamp projects"
  end

  test "Uninstalling Todos" do
    FactoryGirl.create(:addon, name: "todos", enabled: true, locker: @locker)
    signin(@user)

    visit locker_addons_path(@locker)
    click_on "Installed Apps"

    assert_content "Todos"

    click_on "Uninstall"

    refute_content "Todos"
  end

  test "Installed addons aren't listed" do
    FactoryGirl.create(:addon, name: "todos", enabled: true, locker: @locker)
    signin(@user)

    visit locker_addons_path(@locker)

    refute_content "Todos"
  end
end
