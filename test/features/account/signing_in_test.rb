require "test_helper"

class SigningInTest < Capybara::Rails::TestCase
  def setup
    @user = FactoryGirl.create(:user)
  end

  test "Signing in with valid credentials" do
    visit signin_path

    fill_in "Email", with: @user.email
    fill_in "Password", with: @user.password
    click_button "Sign In"

    assert_content page, "Lockers"
  end

  test "Signing in with invalid credentials" do
    visit signin_path

    fill_in "Email", with: @user.email
    fill_in "Password", with: "wrong_password"
    click_button "Sign In"

    assert_content page, "Sign in to your account"
  end
end
