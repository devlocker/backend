require "test_helper"

class SigningOutTest < Capybara::Rails::TestCase
  def setup
    @user = FactoryGirl.create(:user)
  end

  test "Signing out" do
    visit signin_path

    fill_in "Email", with: @user.email
    fill_in "Password", with: @user.password
    click_button "Sign In"

    visit "/signout"

    assert_match /signin/, current_path
  end
end
