require "test_helper"

class ManagingLockersTest < Capybara::Rails::TestCase
  def setup
    basic_setup
  end

  test "Creating a new locker" do
    signin(@user)

    visit new_locker_path

    fill_in "Name", with: "ticketlocker"
    click_on "Save"
    visit lockers_path

    assert_content page, "ticketlocker"
  end

  test "Deleting a locker" do
    signin(@user)

    visit lockers_path
    click_on("Delete")

    assert_equal 0, @user.lockers.count
  end
end
