require "test_helper"

class ResetPasswordMailerTest < ActionMailer::TestCase
  def setup
    basic_setup
  end

  test "reset_password" do
    email = ResetPasswordMailer.reset_password(@user, "password").deliver_now

    refute_empty ActionMailer::Base.deliveries
    assert_equal ["support@devlocker.com"], email.from
    assert_equal [@user.email], email.to
    assert_equal "DevLocker Password Reset", email.subject
  end
end
