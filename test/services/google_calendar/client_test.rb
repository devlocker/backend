require "test_helper"

class GoogleCalendar::ClientTest < ActiveSupport::TestCase
  def setup
    @client = GoogleCalendar::Client.new(
      addon: OpenStruct.new(api_token: "foobar"),
      time_zone: "Pacific Time (US & Canada)"
    )

    stub_request(:get, Regexp.new("https://www.googleapis.com/calendar/v3/calendars/primary/events")).
      to_return(status: 200, body: File.read("test/fixtures/google_calendar/event_list.json"), headers: { "Content-Type" => "application/json" })
  end

  def test_my_issues
    @client.stubs(:setup_authorization)
    assert_equal "What is Agile?", @client.events.last.summary
  end
end
