require "test_helper"

class GithubAddon::ClientTest < ActiveSupport::TestCase
  def setup
    basic_setup
    @addon = FactoryGirl.create(
      :addon,
      name: "github",
      locker: @locker,
      metadata: {
        username: "user"
      }
    )

    @client = GithubAddon::Client.new(addon: @addon)
  end

  def test_repos
    stub_request(
      :get,
      "https://api.github.com/user/repos"
    ).to_return(
      status: 200,
      body: File.read(Rails.root.join("test", "fixtures", "github", "repos_response.json")),
      headers: {}
    )

    assert_equal "dot-files", @client.repos.first.name
  end

  def test_repos_selected
    stub_request(
      :get,
      "https://api.github.com/user/repos"
    ).to_return(
      status: 200,
      body: File.read(Rails.root.join("test", "fixtures", "github", "repos_response.json")),
      headers: {}
    )

    assert_equal 1, @client.repos(selected: ["test"]).count
  end

  def test_pull_requests_with_user
    stub_request(
      :get,
      "https://api.github.com/user/repos"
    ).to_return(
      status: 200,
      body: File.read(Rails.root.join("test", "fixtures", "github", "repos_response.json")),
      headers: {}
    )

    stub_request(
      :get,
      "https://api.github.com/issues?filter=created"
    ).to_return(
      status: 200,
      body: File.read(Rails.root.join("test", "fixtures", "github", "issues_response.json")),
      headers: {}
    )

    assert_equal "Found a bug",
      @client.pull_requests(repos: [1], user: "octocat").first.title
  end
end
