require "test_helper"

class Jira::ClientTest < ActiveSupport::TestCase
  def setup
    basic_setup
    @addon = FactoryGirl.create(
      :addon,
      name: "jira",
      locker: @locker,
      metadata: {
        domain: "https://root.atlassian.net",
        username: "user@example.com",
        cookie: "Cookie"
      }
    )

    @client = Jira::Client.new(addon: @addon)
  end

  def test_my_issues
    stub_request(
      :get,
      "https://root.atlassian.net/rest/api/2/search" +
      "?jql=assignee='user@example.com'"
    ).to_return(
      status: 200,
      body: Rails.root.join(
        "test",
        "fixtures",
        "jira", "my_issues_response.json"
      ),
      headers: { "Content-Type" => "application/json" }
    )

    assert_equal(
      "Finish out controller tests",
      @client.my_issues.first["fields"]["summary"]
    )
  end

  def test_set_session_id
    stub_request(
      :post,
      "https://root.atlassian.net/rest/auth/1/session"
    ).to_return(
      status: 200,
      body: "",
      headers: {
        "server" => ["nginx"],
        "date" => ["Sat, 18 Jul 2015 20:25:51 GMT"],
        "content-type" => ["application/json;charset=UTF-8"],
        "transfer-encoding" => ["chunked"],
        "connection" => ["close"],
        "vary" => ["Accept-Encoding"],
        "x-arequestid" => ["805x255030x2"],
        "x-asen" => ["SEN-2714298"],
        "set-cookie" => ["atlassian.xsrf.token=BMF7-XC2W-CVQ6-8AEF|b8fc075e9783e349ec2d1d53bf29f5a78f5017e3|lout; Path=/; Secure", "JSESSIONID=4D18D58FC99546EEDE4955E009FB3B0C; Path=/; Secure; HttpOnly", "studio.crowd.tokenkey=\"\"; Domain=.procoretech.atlassian.net; Expires=Thu, 01-Jan-1970 00:00:10 GMT; Path=/; Secure; HttpOnly", "studio.crowd.tokenkey=34wxJWJPuNuzcED2S0SS7g00; Domain=.procoretech.atlassian.net; Path=/; Secure; HttpOnly"],
        "x-ausername" => ["anonymous"],
        "x-atenant-id" => ["procoretech.atlassian.net"],
        "x-seraph-loginreason" => ["OUT", "OK"],
        "cache-control" => ["no-cache, no-store, no-transform"],
        "x-content-type-options" => ["nosniff"],
        "strict-transport-security" => ["max-age=315360000;includeSubDomains"]
      }
    )

    @client.set_session_id("username", "password")

    assert @addon.reload.metadata["cookie"].match /JSESSIONID/
  end
end
