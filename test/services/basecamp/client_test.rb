require "test_helper"

class Basecamp::ClientTest < ActiveSupport::TestCase
  def setup
    stub_request(
      :get,
      "https://launchpad.37signals.com/authorization.json"
    ).to_return(
      status: 200,
      body: load_basecamp_fixture("accounts_response.json"),
      headers: { "Content-Type" => "application/json" }
    )

    stub_request(
      :get,
      "https://basecamp.com/2013938/api/v1/projects.json"
    ).to_return(
      status: 200,
      body: load_basecamp_fixture("projects_response.json"),
      headers: { "Content-Type" => "application/json" }
    )

    basic_setup
    addon = FactoryGirl.create(
      :addon,
      locker: @locker,
      name: "basecamp",
      api_token: "token"
    )

    @client = Basecamp::Client.new(addon: addon)
  end

  def test_todolists
    stub_request(:get, "https://basecamp.com/2013938/api/v1/projects/4239710/todolists.json").
      with(
        headers: { 'Authorization'=>'Bearer token', 'Content-Type'=>'application/json; charset=utf-8', 'User-Agent'=>'DevLocker' }
      ).
      to_return(
        status: 200,
        body: load_basecamp_fixture("todolists_response.json"),
        headers: { "Content-Type" => "application/json" }
      )

    assert_equal "SuperUser", @client.todolists[0]["name"]
  end

  def test_todos
    todolist_id = 4239710

    stub_request(:get, "https://basecamp.com/2013938/api/v1/projects/4239710/todolists/4239710/todos/remaining.json").
      with(
        headers: { 'Authorization'=>'Bearer token', 'Content-Type'=>'application/json; charset=utf-8', 'User-Agent'=>'DevLocker' }
      ).
      to_return(
        status: 200,
        body: load_basecamp_fixture("todos_response.json"),
        headers: { "Content-Type" => "application/json" }
      )

    assert_equal "Achieve feature parity", @client.todos(todolist_id: todolist_id)[0]["content"]
  end

  def load_basecamp_fixture(file)
    File.read(Rails.root.join("test", "fixtures", "basecamp", file))
  end
end
