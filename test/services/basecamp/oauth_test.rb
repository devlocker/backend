require "test_helper"

class OauthTest < ActiveSupport::TestCase
  def setup
    stub_request(
      :post,
      "https://launchpad.37signals.com/authorization/token?client_id=test&" +
      "client_secret=test&code=&redirect_uri=" +
      "http://devlocker.test/basecamp/oauth&type=web_server"
    ).to_return(
      status: 200,
      body: File.read(
        Rails.root.join(
          "test",
          "fixtures",
          "basecamp",
          "oauth_response.json"
        )
      ),
      headers: {
        "Content-Type" => "application/json"
      }
    )
  end

  def test_access_token
    assert_equal "fake_access_token", Basecamp::Oauth.new.access_token
  end

  def test_refresh_token
    assert_equal "fake_refresh_token", Basecamp::Oauth.new.refresh_token
  end

  def test_authorize_url
    assert_equal authorize_url, Basecamp::Oauth.new.authorize_url
  end

  def authorize_url
    "https://launchpad.37signals.com/authorization/new?type=web_server&" +
      "client_id=test&redirect_uri=http://devlocker.test/basecamp/oauth"
  end
end
