require "test_helper"

class TrelloAddon::ClientTest < ActiveSupport::TestCase
  def setup
    stub_request(:any, /api.trello.com/).to_rack(FakeTrello)
    basic_setup
    @addon = FactoryGirl.create(
      :addon,
      name: "trello",
      locker: @locker,
      api_token: "token"
    )
  end

  def test_boards
    member = TrelloAddon::Client.new(addon: @addon)

    assert_equal("546bdb6b463a50d4b2b018c3", member.boards.first.id)
  end

  def test_cards
    member = TrelloAddon::Client.new(addon: @addon)
    board_ids = member.boards.map(&:id)

    assert_equal("547121de5fcd56732edce521", member.cards(board_ids).first.id)
  end
end

require "sinatra/base"

class FakeTrello < Sinatra::Base
  get "/1/members/me" do
    json_response 200, "member_me.json"
  end

  get "/1/members/:id/boards" do
    json_response 200, "boards.json"
  end

  get "/1/members/:id/cards" do
    json_response 200, "cards.json"
  end

  private

  def json_response(response_code, file_name)
    content_type :json
    status response_code
    File.open("#{Rails.root}/test/fixtures/trello/#{file_name}").read
  end
end
