require "test_helper"
require_relative "../../../lib/core_ext/string_refinements.rb"

class TestClassTest < ActiveSupport::TestCase
  using ::StringRefinements

  def test_to_bool_true
    assert "true".to_bool
    assert "TRUE".to_bool
    assert "t".to_bool
  end

  def test_to_bool_false
    refute "false".to_bool
    refute "FALSE".to_bool
    refute "f".to_bool
  end

  def test_to_bool_other
    refute "other".to_bool
    refute "".to_bool
    refute " ".to_bool
  end
end
