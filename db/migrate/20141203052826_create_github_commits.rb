class CreateGithubCommits < ActiveRecord::Migration
  def change
    create_table :github_commits do |t|
      t.text :name
      t.text :message
      t.integer :source_id, null: false, index: true, unique: true
      t.text :url
      t.boolean :distinct
      t.references :github_repo
      t.text :added, array: true, default: []
      t.text :removed, array: true, default: []
      t.text :modified, array: true, default: []
      t.datetime :committed_at

      t.timestamps null: false
    end
  end
end
