class CreateGithubRepoMemberships < ActiveRecord::Migration
  def change
    create_table :github_repo_memberships do |t|
      t.references :user, index: true
      t.references :github_repo, index: true

      t.timestamps null: false
    end
    add_foreign_key :github_repo_memberships, :users
    add_foreign_key :github_repo_memberships, :github_repos
  end
end
