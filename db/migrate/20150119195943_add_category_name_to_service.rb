class AddCategoryNameToService < ActiveRecord::Migration
  def change
    add_column :services, :category_name, :text
  end
end
