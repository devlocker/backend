class AddOwnerToGithubRepo < ActiveRecord::Migration
  def change
    add_column :github_repos, :owner, :text
  end
end
