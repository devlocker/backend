class CreateAddons < ActiveRecord::Migration
  def change
    create_table :addons do |t|
      t.boolean :enabled, null: false, default: false
      t.references :locker, null: false
      t.string :api_token
      t.string :name, null: false
      t.jsonb :metadata, index: true, default: {}

      t.timestamps null: false
    end
  end
end
