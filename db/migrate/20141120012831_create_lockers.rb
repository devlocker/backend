class CreateLockers < ActiveRecord::Migration
  def change
    create_table :lockers do |t|
      t.text :name
      t.belongs_to :user, null: false

      t.timestamps null: false
    end

    add_index :lockers, :user_id
  end
end
