class AddTimestampsToGithubPullRequest < ActiveRecord::Migration
  def change
    add_column :github_pull_requests, :remote_created_at, :datetime
    add_column :github_pull_requests, :remote_updated_at, :datetime
  end
end
