class CreateAuthentications < ActiveRecord::Migration
  def change
    create_table :authentications do |t|
      t.text :token
      t.belongs_to :user, index: true

      t.timestamps null: false
    end
  end
end
