class UpdateGithubIssues < ActiveRecord::Migration
  def change
    add_column :github_issues, :repo_id, :integer, null: false, index: true
  end
end
