class RemoveGithubPullRequests < ActiveRecord::Migration
  def change
    drop_table :github_pull_requests
  end
end
