class CreateGithubIssues < ActiveRecord::Migration
  def change
    create_table :github_issues do |t|
      t.text :title
      t.text :body
      t.text :url
      t.text :labels_url
      t.text :comments_url
      t.text :events_url
      t.text :html_url
      t.integer :source_id
      t.integer :number
      t.integer :state
      t.boolean :locked
      t.datetime :source_created_at
      t.datetime :source_updated_at
      t.datetime :closed_at

      t.timestamps null: false
    end
  end
end
