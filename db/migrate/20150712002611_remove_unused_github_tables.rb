class RemoveUnusedGithubTables < ActiveRecord::Migration
  def change
    drop_table :github_locker_repos
    drop_table :github_repo_memberships
    drop_table :github_commits
    drop_table :github_issues
    drop_table :github_repos
  end
end
