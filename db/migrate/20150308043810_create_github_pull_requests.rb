class CreateGithubPullRequests < ActiveRecord::Migration
  def change
    create_table :github_pull_requests do |t|
      t.text :diff_url
      t.text :patch_url
      t.text :issue_url
      t.integer :number
      t.text :state
      t.boolean :locked
      t.text :title
      t.text :body
      t.text :closed_at
      t.text :merged_at
      t.text :merge_commit_sha
      t.text :assignee
      t.text :milestone
      t.text :commits_url
      t.integer :source_id
      t.references :repo, index: true

      t.timestamps null: false
    end
  end
end
