class CreateTodoItems < ActiveRecord::Migration
  def change
    create_table :todo_items do |t|
      t.references :locker, index: true
      t.text :content, null: false
      t.boolean :completed, default: false, null: false

      t.timestamps null: false
    end
    add_foreign_key :todo_items, :lockers
  end
end
