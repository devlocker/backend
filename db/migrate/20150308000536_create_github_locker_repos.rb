class CreateGithubLockerRepos < ActiveRecord::Migration
  def change
    create_table :github_locker_repos do |t|
      t.references :locker, index: true
      t.references :github_repo, index: true

      t.timestamps null: false
    end

    add_foreign_key :github_locker_repos, :lockers
    add_foreign_key :github_locker_repos, :github_repos
  end
end
