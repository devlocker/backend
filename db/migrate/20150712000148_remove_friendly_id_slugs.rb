class RemoveFriendlyIdSlugs < ActiveRecord::Migration
  def change
    remove_index :friendly_id_slugs, :sluggable_id
    remove_index :friendly_id_slugs, [:slug, :sluggable_type]
    remove_index :friendly_id_slugs, [:slug, :sluggable_type, :scope]
    remove_index :friendly_id_slugs, :sluggable_type
    drop_table :friendly_id_slugs
  end
end
