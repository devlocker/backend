class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.text :name
      t.belongs_to :authentication, index: true
      t.belongs_to :locker, index: true

      t.timestamps null: false
    end
  end
end
