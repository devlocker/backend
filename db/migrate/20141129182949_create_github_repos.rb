class CreateGithubRepos < ActiveRecord::Migration
  def change
    create_table :github_repos do |t|
      t.text :name
      t.integer :source_id, null: false, index: true, unique: true

      t.timestamps null: false
    end
  end
end
