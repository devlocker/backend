class AddTimeZoneToUser < ActiveRecord::Migration
  def change
    add_column :users, :time_zone, :string, default: "Pacific Time (US & Canada)", null: false
  end
end
