class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.text :email, null: false
      t.text :first_name
      t.text :last_name
      t.text :password_digest

      t.timestamps null: false
    end

    User.reset_column_information
    add_index :users, :email, unique: true
  end
end
