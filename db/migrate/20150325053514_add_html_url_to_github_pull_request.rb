class AddHtmlUrlToGithubPullRequest < ActiveRecord::Migration
  def change
    add_column :github_pull_requests, :html_url, :text
  end
end
