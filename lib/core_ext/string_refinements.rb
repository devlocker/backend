module StringRefinements
  refine String do
    def to_bool
      case self.downcase
      when "true", "t" then true
      when "false", "f" then false
      else
        false
      end
    end
  end
end
