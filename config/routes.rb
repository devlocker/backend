Rails.application.routes.draw do
  root "lockers#index"

  get "/signin", to: "sessions#new"
  get "/signout", to: "sessions#destroy"
  delete "/signout", to: "sessions#destroy"

  get "/signup", to: "users#new", as: "signup"
  post "/signup", to: "users#create"

  get "/reset_password", to: "reset_password#new"
  post "/reset_password", to: "reset_password#create"

  resources :users, only: [:edit, :update] do
    collection do
      get "validate_email"
    end
  end

  get "/auth/:provider/callback", to: "authentications#create"
  get "/basecamp/oauth", to: "basecamp/oauth#create"

  resources :sessions, only: [:new, :create, :destroy]
  resources :lockers, except: [:edit], path: "~" do
    resources :addons, only: [:index, :new, :create, :show, :destroy] do
      get :installed, on: :collection
      patch :update_color, on: :member
    end

    namespace :github do
      resources :repos, only: [:index, :create, :update] do
        post "/webhook", action: :webhook, on: :collection
      end
      resources :commits, only: [:index]
      resources :issues, only: [:index, :create]
      resources :pull_requests, only: [:index, :create]
    end

    namespace :todo do
      resources :items
    end

    namespace :google_calendar do
      resources :items, only: [:index]
    end

    namespace :jira do
      resources :items, only: [:index]
    end

    namespace :basecamp do
      resources :oauth, only: [:new]
      resources :todos, only: [:index] do
        post :complete, on: :member
      end
      resources :todolists, only: [:index]
    end

    namespace :bugsnag do
      resources :errors, only: [] do
        collection do
          get :common
          get :recent
        end
      end
    end
  end
end
