Rails.application.config.middleware.use OmniAuth::Builder do
  provider :github, Rails.application.secrets.github_client_id, Rails.application.secrets.github_client_secret, app_name: "DevLocker", scope: "admin:repo_hook, repo"
  provider :trello, Rails.application.secrets.trello_client_id, Rails.application.secrets.trello_client_secret, app_name: "DevLocker"
  provider :google_oauth2, Rails.application.secrets.google_client_id, Rails.application.secrets.google_client_secret, {
    app_name: "DevLocker",
    scope: "email, profile, calendar.readonly",
    name: "google",
    prompt: "consent select_account",
    access_type: "offline"
  }
end
