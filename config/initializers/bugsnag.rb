if defined?(Bugsnag)
  Bugsnag.configure do |config|
    config.api_key = Rails.application.secrets.bugsnag_key
  end
end
