module ApplicationHelper
  def current_locker
    id = params[:locker_id] || params[:id]

    @current_locker ||= current_user.lockers.find_by(id: id) || NullLocker.new
  end

  def long_time
    Time.now.in_time_zone(current_user.time_zone).to_formatted_s(:long)
  end
end
