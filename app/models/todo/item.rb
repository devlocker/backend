# == Schema Information
#
# Table name: todo_items
#
#  id         :integer          not null, primary key
#  locker_id  :integer
#  content    :text             not null
#  completed  :boolean          default(FALSE), not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_todo_items_on_locker_id  (locker_id)
#

class Todo::Item < ActiveRecord::Base
  belongs_to :locker
end
