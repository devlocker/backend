# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  email           :text             not null
#  first_name      :text
#  last_name       :text
#  password_digest :text
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  time_zone       :string           default("Pacific Time (US & Canada)"), not null
#
# Indexes
#
#  index_users_on_email  (email) UNIQUE
#

class User < ActiveRecord::Base
  has_secure_password

  has_many :lockers
  has_many :authentications

  def name
    "#{first_name} #{last_name}".presence || email
  end

  def signed_in?
    true
  end
end
