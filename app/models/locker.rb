# == Schema Information
#
# Table name: lockers
#
#  id         :integer          not null, primary key
#  name       :text
#  user_id    :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_lockers_on_user_id  (user_id)
#

class Locker < ActiveRecord::Base
  belongs_to :user

  has_many :addons, dependent: :destroy
  has_many :todo_items, class_name: Todo::Item, dependent: :destroy
end
