class AddonsList
  def self.all
    productivity + project_management
  end

  def self.productivity
    [
      {
        name: "todos",
        description: "A simple todo list",
        category_name: "Productivity"
      },
      {
        name: "google",
        description: "Time-management web application that helps you organize events",
        category_name: "Productivity"
      },
    ]
  end

  def self.project_management
    [
      {
        name: "github",
        description: "Track and collaborate on your code",
        category_name: "Project Management"
      },
      {
        name: "basecamp",
        description: "Basecamp helps you wrangle people with different roles, responsibilities, and objectives toward a common goal: Finishing a project together",
        category_name: "Project Management"
      },
      {
        name: "jira",
        description: "Is a proprietary issue tracking product, developed by Atlassian. It provides bug tracking, issue tracking, and project management ",
        category_name: "Project Management"
      },
    ]
  end
end
