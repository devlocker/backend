# == Schema Information
#
# Table name: addons
#
#  id         :integer          not null, primary key
#  enabled    :boolean          default(FALSE), not null
#  locker_id  :integer          not null
#  api_token  :string
#  name       :string           not null
#  metadata   :jsonb            default({})
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_addons_on_metadata  (metadata)
#

class MetadataValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    if value["domain"]
      unless value["domain"] =~ /https:\/\//
        record.errors[attribute] << "Please use the https protocol for domains"
      end
    end
  end
end

class Addon < ActiveRecord::Base
  belongs_to :locker

  validates :metadata, metadata: true

  scope :enabled, -> { where(enabled: true) }

  def color
    metadata["color"] || "color-1"
  end

  def post_create(*args)
    if name == "jira"
      Jira::Client.new(addon: self).set_session_id(args[0], args[1])
    end
  end
end
