class Jira::ItemsController < ApiController
  def index
    @items = cache(issues_cache_key) do
      client.my_issues
    end
  end

  private

  def client
    @client ||= Jira::Client.new(addon: jira_addon)
  end

  def jira_addon
    current_locker.addons.find_by!(name: "jira")
  end

  def issues_cache_key
    "#{current_user.id}/#{current_locker.id}/jira/issues"
  end
end
