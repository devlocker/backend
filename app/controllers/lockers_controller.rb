class LockersController < ApplicationController
  def index
    @lockers = current_user.lockers
  end

  def new
    @locker = current_user.lockers.new
  end

  def show
    locker = current_user.lockers.find(params[:id])
    @enabled_addons = locker.addons.enabled.map { |addon| addon.name.camelize + "App" }
  end

  def create
    @locker = current_user.lockers.new(locker_params)

    if @locker.save
      redirect_to @locker
    else
      render :new
    end
  end

  def destroy
    @locker = current_user.lockers.find(params[:id])
    @locker.destroy

    redirect_to action: :index
  end

  private

  def locker_params
    params.require(:locker).permit(:name)
  end
end
