class UsersController < ApplicationController
  skip_before_filter :authorize, only: [:new, :create, :validate_email]
  layout "account", only: [:new, :create, :validate_email]

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)

    respond_to do |format|
      format.html do
        if @user.save
          sign_in(@user)
          redirect_to lockers_path
        else
          render :new
        end
      end
      format.json do
        if @user.save
          sign_in(@user)
          render json: @user.to_json, status: :ok
        else
          head :bad_request
        end
      end
    end
  end

  def edit
    @user = current_user
  end

  def update
    @user = current_user

    if @user.update(user_params)
      redirect_to lockers_path
    else
      flash[:error] = @user.errors.full_messages.join(" ,")
      render :edit
    end
  end

  def validate_email
    render json: { exists: User.exists?(email: validate_email_params) }
  end

  private

  def user_params
    params.require(:user).permit(
      :email,
      :first_name,
      :last_name,
      :password,
      :password_confirmation,
      :time_zone
    )
  end

  def validate_email_params
    params.require(:email)
  end
end
