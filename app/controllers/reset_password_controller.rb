class ResetPasswordController < ApplicationController
  skip_before_filter :authorize

  def new
  end

  def create
    user = User.find_by(email: params[:reset][:email]) || GuestUser.new

    if user.update(password: new_password, password_confirmation: new_password)
      ResetPasswordMailer.reset_password(user, new_password).deliver_now
      redirect_to signin_path
    else
      flash[:error] = "You gave us an invalid login, please try again"
      redirect_to reset_password_path
    end
  end

  private

  def new_password
    @password ||= SecureRandom.hex(16)
  end
end
