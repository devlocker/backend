class SessionsController < ApplicationController
  skip_before_filter :authorize

  def new
  end

  def create
    user = User.find_by(email: session_params[:email])
    if user && user.authenticate(session_params[:password])
      sign_in(user)
      redirect_to lockers_path
    else
      flash[:error] = "Incorrect username or password"
      render :new
    end
  end

  def destroy
    session[:user_id] = nil

    redirect_to signin_path
  end

  private

  def session_params
    params.require(:session).permit(:email, :password)
  end
end
