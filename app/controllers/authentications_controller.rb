class AuthenticationsController < ApplicationController
  def create
    current_addon.update!(
      api_token: token,
      metadata: current_addon.metadata.merge(username: username),
      enabled: true
    )

    redirect_to current_locker
  end

  private

  def current_locker
    current_user.lockers.find(locker_id)
  end

  def current_addon
    current_locker.
      addons.
      where(name: omniauth_hash[:provider]).
      first_or_initialize
  end

  def token
    if refresh_token
      refresh_token
    else
      omniauth_hash.fetch(:credentials, {}).fetch(:token, nil)
    end
  end

  def refresh_token
    omniauth_hash.fetch(:credentials, {}).fetch(:refresh_token, nil)
  end

  def username
    omniauth_hash.fetch(:info, {}).fetch(:nickname, nil)
  end

  def locker_id
    if env.fetch("omniauth.params", {}).fetch("locker_id", nil)
      env["omniauth.params"]["locker_id"]
    elsif env.fetch("omniauth.origin", nil)
      locker_id_from_origin
    else
      params[:locker_id]
    end
  end

  def locker_id_from_origin
    env["omniauth.origin"].split("/")[-4]
  end

  def omniauth_hash
    env.fetch("omniauth.auth", {}).with_indifferent_access
  end
end
