class AddonsController < ApplicationController
  layout "addon"

  def index
    @addons = current_locker.addons.enabled.pluck(:name)
  end

  def installed
    @addons = current_locker.addons.enabled
  end

  def new
    @addon = current_locker.addons.new

    render "new-#{params[:addon_name]}"
  end

  def show
  end

  def create
    @addon = current_locker.addons.new(parsed_addon_params)

    if @addon.save
      @addon.post_create(@addon.metadata["username"], @password)

      redirect_to locker_path(current_locker)
    else
      render :new
    end
  end

  def destroy
    @addon = current_locker.addons.find(params[:id])
    @addon.update!(enabled: false)

    redirect_to action: :installed
  end

  def update_color
    @addon = current_locker.addons.find(params[:id])
    @addon.update!(metadata: @addon.metadata.merge(addon_params[:metadata]))

    head :ok
  end

  private

  def addon_params
    params.require(:addon).permit(
      :api_token,
      :name,
      metadata: [
        :username,
        :password,
        :color,
        :domain
      ]
    )
  end

  def parsed_addon_params
    @password = params[:addon].fetch(:metadata, {}).delete(:password)

    addon_params.merge(enabled: true)
  end
end
