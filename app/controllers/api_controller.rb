require "./lib/core_ext/string_refinements.rb"

class ApiController < ActionController::Base
  using ::StringRefinements

  include ApplicationHelper
  include SessionsHelper

  protect_from_forgery with: :null_session

  before_action :authorize

  private

  def authorize
    unless signed_in?
      redirect_to signin_path
    end
  end

  def cache(cache_key, expires_in: 10.minutes, &block)
    if manual_expire
      Rails.cache.delete(cache_key)
    end

    Rails.cache.fetch(cache_key, expires_in: expires_in) do
      block.call
    end
  end

  def manual_expire
    params.fetch(:manual_expire, false).to_s.to_bool
  end
end
