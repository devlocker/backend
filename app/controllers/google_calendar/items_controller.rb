class GoogleCalendar::ItemsController < ApiController
  def index
    @items = cache(items_cache_key) do
      client.events
    end
  end

  private

  def client
    @client ||= GoogleCalendar::Client.new(
      addon: google_calendar_addon,
      time_zone: time_zone
    )
  end

  def time_zone
    current_user.time_zone || Rails.application.config.time_zone
  end

  def google_calendar_addon
    current_locker.addons.find_by!(name: "google")
  end

  def items_cache_key
    "#{current_user.id}/#{current_locker.id}/google/events"
  end
end
