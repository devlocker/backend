class Github::ReposController < ApiController
  skip_before_filter :authorize, only: [:webhook]

  def index
    @repos = cache(repos_cache_key) do
      client.repos(selected: [])
    end
  end

  def update
    connected_repos = github_addon.metadata["connected_repo_ids"] || []

    case params[:locker_membership]
    when true
      connected_repos.push(params[:id].to_i)
    when false
      connected_repos.delete(params[:id].to_i)
    end

    github_addon.metadata["connected_repo_ids"] = connected_repos
    github_addon.save!

    respond_to do |format|
      format.json { head :ok }
    end
  end

  def webhook
    if objects = Github::Response.build(params)
      objects.map(&:save)
    end

    render json: { success: true }, status: :created
  end

  private

  def github_addon
    @addon ||= current_locker.addons.find_by!(name: "github")
  end

  def client
    @client ||= GithubAddon::Client.new(addon: github_addon)
  end

  def repos_cache_key
    "#{current_user.id}/#{current_locker.id}/github/repos"
  end
end
