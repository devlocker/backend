class Github::PullRequestsController < ApiController
  def index
    @pull_requests = cache(pull_requests_cache_key) do
      client.pull_requests(repos: connected_repo_ids)
    end
  end

  private

  def connected_repo_ids
    github_addon.metadata["connected_repo_ids"] || Array.new
  end

  def github_addon
    @github_addon ||= current_locker.addons.find_by!(name: "github")
  end

  def client
    @github_client ||= GithubAddon::Client.new(addon: github_addon)
  end

  def pull_requests_cache_key
    "#{current_user.id}/#{current_locker.id}/github/pull_requests"
  end
end
