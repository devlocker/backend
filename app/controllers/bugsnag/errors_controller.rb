class Bugsnag::ErrorsController < ApiController
  def common
    @common = cache(errors_cache_key) do
      client.most_common_error
    end
  end

  def recent
    @recent = cache(common_errors_cache_key) do
      client.recent_errors
    end
  end

  private

  def client
    @client ||= Bugsnag::Client.new(addon: bugsnag_addon)
  end

  def bugsnag_addon
    current_locker.addons.find_by!(name: "bugsnag")
  end

  def recent_errors_cache_key
    "#{current_user.id}/#{current_locker.id}/bugsnag/recent_errors"
  end

  def common_errors_cache_key
    "#{current_user.id}/#{current_locker.id}/bugsnag/common_errors"
  end
end
