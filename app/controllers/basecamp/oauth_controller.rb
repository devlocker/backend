class Basecamp::OauthController < ApiController
  def new
    session[:locker_id] = params[:locker_id]

    redirect_to oauth.authorize_url
  end

  def create
    current_addon.update!(
      api_token: oauth.access_token,
      metadata: current_addon.metadata.merge(refresh_token: oauth.refresh_token),
      enabled: true
    )

  ensure
    locker = current_locker
    session.delete(:locker_id)

    redirect_to locker_path(locker)
  end

  private

  def current_locker
    current_user.lockers.find(session[:locker_id].to_i)
  end

  def current_addon
    current_locker.addons.where(name: "basecamp").first_or_initialize
  end

  def oauth
    Basecamp::Oauth.new(params[:code])
  end
end
