class Basecamp::TodolistsController < ApiController
  def index
    @todolists = cache(todolists_cache_key) do
      client.todolists
    end
  end

  private

  def client
    Basecamp::Client.new(addon: basecamp_addon)
  end

  def basecamp_addon
    current_locker.addons.find_by(name: "basecamp")
  end

  def current_locker
    current_user.lockers.find(params[:locker_id])
  end

  def todolists_cache_key
    "#{current_user.id}/#{current_locker.id}/basecamp/todolists"
  end
end
