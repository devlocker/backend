class Basecamp::TodosController < ApiController
  protect_from_forgery with: :null_session

  def index
    @todos = cache(todos_cache_key) do
      client.todos(todolist_id: params[:todolist_id])
    end
  end

  def complete
    client.complete_todo(params[:id])
    head :ok
  end

  private

  def client
    @client ||= Basecamp::Client.new(addon: basecamp_addon)
  end

  def basecamp_addon
    current_locker.addons.find_by!(name: "basecamp")
  end

  def todos_cache_key
    "#{current_user.id}/#{current_locker.id}/basecamp/#{params[:todolist_id]}/todos"
  end
end
