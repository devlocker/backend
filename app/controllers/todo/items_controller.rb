class Todo::ItemsController < ApiController
  def index
    @items = cache(todo_items_cache_key) do
      current_locker.todo_items.order(created_at: :asc)
    end
  end

  def show
    @item = current_locker.todo_items.find(params[:id])
  end

  def create
    @item = current_locker.todo_items.new(todo_item_params)

    if @item.save
      render :show
    else
      render json: { errors: @item.errors }, status: :bad_request
    end
  end

  def update
    @item = current_locker.todo_items.find(params[:id])

    if @item.update(todo_item_params)
      render :show
    else
      render json: { errors: @item.errors }, status: :bad_request
    end
  end

  def destroy
    @item = current_locker.todo_items.find(params[:id])
    @item.destroy

    head :ok
  end

  private

  def todo_item_params
    params.require(:item).permit(:content, :completed)
  end

  def todo_items_cache_key
    "#{current_user.id}/#{current_locker.id}/todo_items"
  end
end
