class GithubAddon::Client
  def initialize(addon:)
    @addon = addon
    @client = setup_client
  end

  def repos(selected: Array.new)
    @repos ||= if selected.any?
                 all_repos.select { |repo| selected.include?(repo.name) }
               else
                 all_repos
               end
  end

  def pull_requests(repos:, user: nil)
    client.issues.list(filter: "created").select do |issue|
      issue.has_key?("pull_request") && repos.include?(issue.repository.id)
    end
  end

  private
  attr_reader :addon, :client

  def all_repos
    @all_repos ||= client.repos.list
  end

  def setup_client
    Github.new do |c|
      c.oauth_token = addon.api_token
      c.user_agent = "DevLocker"
      c.auto_pagination = true
    end
  end
end
