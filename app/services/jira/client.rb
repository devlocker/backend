class Jira::Client
  def initialize(addon:)
    @addon = addon
  end

  def my_issues
    # retry_count ||= 1
    response = HTTParty.get(my_issues_url, headers: { "Cookie" => addon.metadata["cookie"] })

    if response.success?
      response.fetch("issues") { [] }
      # else
      #   if retry_count > 0
      #     # prompt the user for username password
      #     set_session_id
      #     retry
      #   end
    end
  end

  def set_session_id(username, password)
    # NOTE The client url must use "https" protocol for this endpoint to work,
    # this is enforced when they sign up for the addon
    response = HTTParty.post(
      auth_url,
      body: {
        "username" => username,
        "password" => password
      }.to_json,
      headers: { "Content-Type" => "application/json" }
    )

    set_cookie = response.headers["Set-Cookie"].split(",")
    session_headers = build_session_headers(set_cookie)

    addon.update!(metadata: addon.metadata.merge(cookie: session_headers))
  end

  private
  attr_reader :addon

  def my_issues_url
    root_url + "/search?jql=assignee='#{addon.metadata['username']}'"
  end

  def root_url
    addon.metadata["domain"] + "/rest/api/2"
  end

  def auth_url
    addon.metadata["domain"] + "/rest/auth/1/session"
  end

  def build_session_headers(set_cookie)
    session_headers = set_cookie.map do |header|
      unless header =~ /studio.crowd.tokenkey=\"\"/ || header =~ /01-Jan-1970/
        header
      end
    end

    session_headers.compact.join(",")
  end
end
