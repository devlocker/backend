class Bugsnag::Client
  def initialize(addon:)
    @addon = addon
  end

  def most_common_error
    [all_errors.max_by { |error| error["occurrences"] }]
  end

  def recent_errors
    all_errors.first(3)
  end

  private
  attr_reader :addon

  def all_errors
    selected_projects.flat_map do |id|
      JSON.parse(HTTParty.get(errors_url(id), headers: headers).body)
    end
  end

  def projects
    selected_accounts.map do |id|
      HTTParty.get(projects_url(id), headers: headers)
    end
  end

  def accounts
    HTTParty.get(accounts_url, headers: headers)
  end

  def errors_url(project_id)
    root_url + "/projects/#{project_id}/errors"
  end

  def projects_url(account_id)
    root_url + "/accounts/#{account_id}/projects"
  end

  def accounts_url
    root_url + "/accounts"
  end

  def root_url
    "https://api.bugsnag.com"
  end

  def selected_accounts
    addon.metadata["selected_accounts"]
    ["55df89cd77656206d80021cb"]
  end

  def selected_projects
    addon.metadata["selected_projects"]
    ["55df89db776562068f0022e1"]
  end

  def headers
    {
      "Authorization" => "token #{token}",
      "User-Agent" => "DevLocker",
      "Content-Type" => "application/json",
      "Accept" => "application/json"
    }
  end

  def token
    addon.api_token
    "d0817ea7fc892de92ded056a17f55a13"
  end
end
