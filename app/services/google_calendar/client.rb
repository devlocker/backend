require "google/apis/calendar_v3"

class GoogleCalendar::Client
  TOKEN_CRED_URI = "https://www.googleapis.com/oauth2/v3/token"

  def initialize(addon:, time_zone:)
    @addon = addon
    @time_zone = time_zone
  end

  def events
    calendar.
      list_events(
        "primary",
        max_results: 10,
        single_events: true,
        order_by: "startTime",
        time_min: Time.now.in_time_zone(time_zone).beginning_of_day.iso8601,
        time_max: Time.now.in_time_zone(time_zone).end_of_day.iso8601
    ).items
  end

  private
  attr_reader :addon, :time_zone

  def calendar
    @calendar ||= setup_calendar
  end

  def setup_calendar
    calendar = Google::Apis::CalendarV3::CalendarService.new
    calendar.authorization = setup_authorization
    calendar
  end

  def setup_authorization
    Signet::OAuth2::Client.new(
      token_credential_uri: TOKEN_CRED_URI,
      client_id: Rails.application.secrets.google_client_id,
      client_secret: Rails.application.secrets.google_client_secret,
      refresh_token: refresh_token,
      scope: [Google::Apis::CalendarV3::AUTH_CALENDAR_READONLY]
    )
  end

  def refresh_token
    addon.api_token
  end
end
