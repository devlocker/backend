class Basecamp::Client
  def initialize(addon:)
    @addon = addon
  end

  def todos(todolist_id:)
    JSON.parse(HTTParty.get(remaining_todos_url(todolist_id), headers: headers).body)
  end

  def todolists
    JSON.parse(HTTParty.get(todolists_url, headers: headers).body)
  end

  def complete_todo(id)
    JSON.parse(
      HTTParty.put(
        todo_url(id),
        body: { completed: true }.to_json,
        headers: headers
      ).body
    )
  end

  private
  attr_reader :addon

  def projects
    @projects ||= HTTParty.get(projects_url, headers: headers)
  end

  def accounts
    @accounts ||= HTTParty.get(accounts_url, headers: headers).fetch("accounts")
  end

  def project_ids
    map_ids(projects)
  end

  def account_ids
    map_ids(accounts)
  end

  def map_ids(collection)
    collection.map { |object| object["id"] }
  end

  def todolists_url
    root_url + "/projects/#{project_ids.first}/todolists.json"
  end

  def todo_url(id)
    root_url + "/projects/#{project_ids.first}/todos/#{id}.json"
  end

  def remaining_todos_url(todolist_id)
    root_url + "/projects/#{project_ids.first}/todolists/#{todolist_id}/todos/remaining.json"
  end

  def projects_url
    root_url + "/projects.json"
  end

  def root_url
    "https://basecamp.com/#{account_ids.first}/api/v1"
  end

  def accounts_url
    "https://launchpad.37signals.com/authorization.json"
  end

  def headers
    {
      "Authorization" => "Bearer #{addon.api_token}",
      "User-Agent" => "DevLocker",
      "Content-Type" => "application/json; charset=utf-8"
    }
  end
end
