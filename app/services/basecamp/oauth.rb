class Basecamp::Oauth
  attr_reader :code

  def initialize(code = nil)
    @code = code
  end

  def access_token
    get_tokens["access_token"]
  end

  def refresh_token
    get_tokens["refresh_token"]
  end

  def authorize_url
    "https://launchpad.37signals.com/authorization/new?type=web_server&" +
      "client_id=#{client_id}&redirect_uri=#{redirect_uri}"
  end

  private

  def get_tokens
    @tokens ||= HTTParty.post(access_token_url)
  end

  def access_token_url
    "https://launchpad.37signals.com/authorization/token?type=web_server&" +
      "client_id=#{client_id}&redirect_uri=#{redirect_uri}&client_secret=" +
      "#{client_secret}&code=#{code}"
  end

  def client_id
    Rails.application.secrets.basecamp_client_id
  end

  def client_secret
    Rails.application.secrets.basecamp_client_secret
  end

  def redirect_uri
    Rails.application.routes.url_helpers.basecamp_oauth_url(host: HOST)
  end
end
