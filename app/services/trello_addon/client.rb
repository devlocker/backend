require "trello"

class TrelloAddon::Client
  def initialize(addon:)
    @member = setup_member(addon)
  end

  def boards
    member.boards
  end

  def cards(board_ids = [])
    member.cards.select { |card| board_ids.include?(card.board_id) }
  end

  attr_reader :member

  private

  def setup_member(addon)
    Trello::Client.new(
      consumer_key: Rails.application.secrets.trello_public_key,
      consumer_secret: Rails.application.secrets.trello_client_secret,
      oauth_token: addon.api_token,
      oauth_token_secret: nil
    ).find("members", "me")
  end
end
