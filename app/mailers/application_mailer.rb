class ApplicationMailer < ActionMailer::Base
  default from: "support@devlocker.com"
  layout "mailer"
end
