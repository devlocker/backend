class ResetPasswordMailer < ApplicationMailer
  def reset_password(user, new_password)
    @user = user
    @new_password = new_password

    mail(to: @user.email, subject: "DevLocker Password Reset")
  end
end
