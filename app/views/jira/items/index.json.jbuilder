json.array! @items do |item|
  json.description item["fields"]["description"]
  json.summary item["fields"]["summary"]
end
