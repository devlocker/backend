json.array! @items do |item|
  json.id        item.id
  json.summary   item.summary
  json.start     item.start.date_time
  json.html_link item.html_link
end
