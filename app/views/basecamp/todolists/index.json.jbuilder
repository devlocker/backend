@todolists.map do |todolist|
  json.set! todolist["id"] do
    json.id      todolist["id"]
    json.name    todolist["name"]
    json.app_url todolist["app_url"]
  end
end
