@todos.map do |todo|
  json.set! todo["id"] do
    json.id        todo["id"]
    json.content   todo["content"]
    json.app_url   todo["app_url"]
    json.completed todo["completed"]
  end
end
