json.array! @pull_requests do |pull_request|
  json.title    pull_request.title
  json.repo     pull_request.repository.full_name
  json.state    pull_request.state
  json.number   pull_request.number
  json.html_url pull_request.html_url
  json.assignee pull_request.assignee
end
