json.array! @commits do |commit|
  json.id        commit.id
  json.message   commit.message
  json.source_id commit.source_id
  json.url       commit.url
  json.distinct  commit.distinct

  json.added commit.added do |filename|
    json.filename filename
  end

  json.removed commit.removed do |filename|
    json.filename filename
  end

  json.modified commit.modified do |filename|
    json.filename filename
  end
end
