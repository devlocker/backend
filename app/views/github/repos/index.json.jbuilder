json.array! @repos do |repo|
  json.id    repo.id
  json.name  repo.full_name
  json.locker_membership @addon.metadata.fetch("connected_repo_ids", []).include?(repo.id)
end
