json.id    @repo.id
json.owner @repo.owner
json.name  @repo.name
json.locker_membership @repo.locker_membership?(current_locker)
