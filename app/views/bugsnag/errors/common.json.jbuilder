json.array! @common do |error|
  json.partial! "bugsnag/errors/error", error: error
end
