json.class          error["class"]
json.last_message   error["last_message"]
json.occurrences    error["occurrences"]
json.users_affected error["users_affected"]
json.last_received  error["last_received"]
json.html_url       error["html_url"]
