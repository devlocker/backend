## DevLocker

A tool for increasing developer productivity

[![Circle CI](https://circleci.com/gh/devlocker/devlocker/tree/master.svg?style=svg)](https://circleci.com/gh/devlocker/devlocker/tree/master)
[![Code Climate](https://codeclimate.com/github/devlocker/devlocker/badges/gpa.svg)](https://codeclimate.com/github/devlocker/devlocker)
